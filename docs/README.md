# 关于 vue-automation

## 这是什么

一款开箱即用的 Vue 项目模版，基于 Vue CLI

## 特点

- 默认集成 vue-router 和 vuex
- 全局 SASS 资源自动引入
- 精灵图自动生成
- 全局组件自动注册
- CDN支持，优化打包体积
- 轻松实现团队代码规范

## 支持

给个小 ❤️ 吧~

[![star](https://img.shields.io/github/stars/hooray/vue-automation?style=social)](https://github.com/hooray/vue-automation/stargazers)

[![star](https://gitee.com/eoner/vue-automation/badge/star.svg?theme=dark)](https://gitee.com/eoner/vue-automation/stargazers)

## 生态

[![](https://hooray.gitee.io/fantastic-admin/logo.png)](https://hooray.gitee.io/fantastic-admin)

[Fantastic-admin](https://hooray.gitee.io/fantastic-admin)

一款开箱即用的 Vue 中后台管理系统框架
